let Usuario = require("../models/Usuario");

module.exports = {
    // Muestra todos los usuarios en la vista index
    list: function(req, res, next){     
        Usuario.find({}, function(err,usuarios) {
            if(err) res.send(500, err.message);
            res.render("usuarios/index", {usuarios: usuarios});
        });
    },

    // Devuelve el usuario con la id de la URL a la vista update
    update_get: function(req, res, next) {
        Usuario.findById(req.params.id, function (err, usuario) {
            res.render("usuarios/update", {errors:{}, usuario: usuario});
        });
    },

    // Actualiza el usuario con la id que se pasa por URL
    update: function(req, res, next){
        let update_values = {nombre: req.body.nombre};
        Usuario.findByIdAndUpdate(req.params.id, update_values, function(err, usuario) {
            // Si hay errores se envía a la misma vista un usuario nuevo con el nombre y email nuevos y los errores
            if (err) {
                console.log(err);
                res.render("usuarios/update", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } else {
                res.redirect("/usuarios");
                return;
            }
        });
    },

    // Muestra la vista create
    create_get: function(req, res, next){
        res.render("usuarios/create", {errors:{}, usuario: new Usuario()});
    },

    // Crea un usuario nuevo
    create: function(req, res, next) {
        // Comprueba que se ha escrito la misma contraseña al confirmarla en el formulario
        // Si son distintas se envían los cambios y los errores a la misma vista
        if (req.body.password != req.body.confirm_password) {
            res.render("usuarios/create", {errors: {confirm_password: {message: "No coincide con el password introducido."}}, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            return;
        }
        Usuario.create({nombre:req.body.nombre, email: req.body.email, password: req.body.password}, function(err, nuevoUsuario) {
            // Si hay algún error al crear el usuario se reenvían los datos a la vista y se muestra el error
            if(err){
                res.render("usuarios/create", {errors: err.errors, usuario: new Usuario({nombre: req.body.nombre, email: req.body.email})});
            } else {
                nuevoUsuario.enviar_email_bienvenida();
                res.redirect("/usuarios");
            }
        });
    },

    // Elimina un usuario
    delete: function(req,res,next){
        Usuario.findByIdAndDelete(req.body.id, function(err){
            if(err)
                next(err);
            else   
                res.redirect("/usuarios");
        });
    }
};
// Importamos el modelo con el objeto Bicicleta
let Bicicleta = require("../models/Bicicleta");

// Con esto pedimos que se muestre una vista index a la que le pasamos
// el array con todas las Bicicleta creadas
exports.bicicleta_list = function(req,res){
     Bicicleta.allBicis(function (err, bicicletas) {
          if (err) res.send(err.message);

          res.render("bicicletas/index", {bicis: bicicletas});
     });
};

exports.bicicleta_create_get = function(req, res) {
     res.render("bicicletas/create");
}

exports.bicicleta_create_post = function(req, res) {
     let bici = new Bicicleta({
          bicicletaID: req.body.bicicletaID,
          color: req.body.color,
          modelo: req.body.modelo,
          ubicacion: [req.body.latitud, req.body.longitud]
     });
     Bicicleta.add(bici, function (err, newBici) {
          if (err) res.send(err.message);
     });
     res.redirect("/bicicletas");
}

exports.bicicleta_delete_post = function(req, res) {
     Bicicleta.removeById(req.body.bicicletaID, function (err, bici) {
          if (err) res.send(err.message);
     });
     res.redirect("/bicicletas");
}

exports.bicicleta_update_get = function(req, res) {
     let biciID = req.params.id;
     Bicicleta.findById(biciID, function (err, bici) {
          if (err) res.send(err.message);

          res.render("bicicletas/update", {bici});
     });
}

exports.bicicleta_update_post = function(req, res) {
     Bicicleta.update(req.params.id, {
          bicicletaID: req.body.bicicletaID,
          color: req.body.color,
          modelo: req.body.modelo,
          ubicacion: [req.body.latitud, req.body.longitud]
     }, function (err, bici) {
          if (err) res.send(err.message);
     });
     res.redirect("/bicicletas");
}
let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis(function(err, bicis) {
        if (err) res.status(500).send(err.message);
        
        res.status(200).json ({
            bicicletas: bicis
        });
    });
};

exports.bicicleta_create = function(req, res) {
    let bici = new Bicicleta({
        bicicletaID: req.body.bicicletaID,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: req.body.ubicacion
    });

    Bicicleta.add(bici, function (err, newBici) {
        if (err) res.status(500).send(err.message);

        res.status(201).send(newBici);
    });
};

exports.bicicleta_delete = function(req, res) {
    let biciId = req.body.bicicletaID;
    Bicicleta.removeById(biciId, function (err, biciId) {
        if (err) res.status(500).send(err.message);

        res.status(204).send();
    });
};

exports.bicicleta_put = function(req, res) {
    let biciId = req.body.bicicletaID;

    Bicicleta.update(biciId, req.body, function (err, bici) {
        if (err) res.status(500).send(err.message);

        bici = req.body;
        res.status(200).json ({
            bicicleta: bici
        });
    });
};
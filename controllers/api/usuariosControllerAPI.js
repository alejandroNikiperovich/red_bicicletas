let Usuario = require("../../models/Usuario.js");

exports.usuarios_list = function(req, res) {
    Usuario.allUsuarios(function(err, users) {
        if (err) res.status(500).send(err.message);
        
        res.status(200).json ({
            usuarios: users
        });
    });
};

exports.usuarios_create = function(req, res) {
    let usuario = new Usuario({
        nombre: req.body.nombre
    });

    Usuario.add(usuario, function (err, newUser) {
        if (err) res.status(500).send(err.message);

        res.status(201).send(newUser);
    });
};

exports.usuarios_delete = function(req, res) {
    let userId = req.body._id;
    Usuario.removeById(userId, function (err, userId) {
        if (err) res.status(500).send(err.message);

        res.status(204).send();
    });
};

exports.usuarios_put = function(req, res) {
    let userId = req.body._id;

    Usuario.update(userId, req.body, function (err, user) {
        if (err) res.status(500).send(err.message);

        user = req.body;
        res.status(200).json ({
            usuario: user
        });
    });
};

exports.usuarios_reservar = function(req, res) {
    Usuario.findById(req.body._id, function (err, usuario) {
        if (err) res.status(500).send(err.message);

        console.log(usuario);

        usuario.reservar(req.body.bici_id, req.body.desde, req.body.hasta, function(err) {
            console.log("Reservada!!");
            res.status(200).send();
        });
    });
};
const moment = require("moment");
let Reserva = require("../../models/Reserva.js");

exports.reservas_list = function(req, res) {
    Reserva.allReservas(function(err, reserva) {
        if (err) res.status(500).send(err.message);
        
        res.status(200).json ({
            reservas: reserva
        });
    });
};

exports.reservas_create = function(req, res) {
    let desde = moment().format(req.body.desde);
    let hasta = moment().format(req.body.hasta);

    let reserva = new Reserva({
        desde: desde,
        hasta: hasta,
        bicicleta: req.body.bicicletaID,
        usuario: req.body.usuarioID
    });

    Reserva.add(reserva, function (err, newReserva) {
        if (err) res.status(500).send(err.message);

        res.status(201).send(newReserva);
    });
};

exports.reservas_delete = function(req, res) {
    let reservaId = req.body._id;
    Reserva.removeById(reservaId, function (err, reserva) {
        if (err) res.status(500).send(err.message);

        res.status(204).send();
    });
};

exports.reservas_put = function(req, res) {
    let reservaId = req.body._id;

    Reserva.update(reservaId, req.body, function (err, reserva) {
        if (err) res.status(500).send(err.message);

        let desde = moment().format(req.body.desde);
        let hasta = moment().format(req.body.hasta);
        reserva = {
            desde: desde,
            hasta: hasta,
            bicicleta: req.body.bicicletaID,
            usuario: req.body.usuarioID
        };

        res.status(200).json ({
            reserva: reserva
        });
    });
};
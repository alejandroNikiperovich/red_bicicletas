const moment = require("moment");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const nodemailer = require("nodemailer");
const uniqueValidator = require("mongoose-unique-validator");
const crypto = require("crypto");
let mailer = require("../mailer/mailer.js");
let Reserva = require("./Reserva.js");
let Token = require("./Token.js");

let saltRounds = 10;

let validateEmail = function(email) {
    let re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/;
    return re.test(email);
}

let Schema = mongoose.Schema;
let usuarioSchema = new Schema ({
    nombre: {
        type: String,
        trim: true,
        required: [true, "El nombre es obligatorio"]
    },

    email: {
        type: String,
        trim: true,
        required: [true, "El email es obligatorio"],
        lowercase: true,
        unique: true,
        validate: [validateEmail, "Por favor, introduzca un email válido"],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/]
    },

    password: {
        type: String,
        required: [true, "El password es obligatorio"]
    },

    passwordResetToken: String,

    passwordResetTokenExpires: Date,

    verificado: {
        type: Boolean,
        default: false
    }
});

usuarioSchema.plugin(uniqueValidator, {message: "El email ya existe con otro ususario."});

usuarioSchema.pre("save", function(next) {
    if (this.isModified("password")) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }

    next();
});

usuarioSchema.statics.allUsuarios = function (cb) {
    return this.find({}, cb);
};

usuarioSchema.statics.add = function(user, cb) {
    return this.create(user, cb);
};

usuarioSchema.statics.findById = function(userId, cb) {
    return this.findOne({_id: userId}, cb);
};

usuarioSchema.statics.removeById = function(userId, cb) {
    return this.findOneAndDelete({_id: userId}, cb);
};

usuarioSchema.statics.update = function(userId, cambios, cb) {
    return this.findOneAndUpdate({_id: userId}, cambios, cb);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
    desde = moment().format(desde);
    hasta = moment().format(hasta);
    let reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync (password, this.password);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    let token = new Token({
        _userId: this.id,
        token: crypto.randomBytes(16).toString("hex")
    });

    let email_destination = this.email;

    token.save(function(err) {
        if (err) {
            return console.log(err.message);
        }

        let mailOptions = {
            from: "alejandronikiperovich@gmail.com",
            to: email_destination,
            subject: "Verificación de cuenta",
            text: "Hola,\n\n" + "Por favor, para verificar su cuenta haga " +
                "click en este enlace: \n" + "http://192.168.88.40:3000" + 
                "\/token/confirmation\/" + token.token + ".\n"
        };

        mailer.verify(function(error, success) {
            if (error) {
              console.log(error);
            } else {
              console.log("Server is ready to take our messages");
            }
          });

        mailer.sendMail(mailOptions, function(err, info) {
            if (err) {
                console.log("Error");
                return console.log(err.message);
            }

            console.log("Error");
            console.log("Se ha enviado un email de bienvenida a " + email_destination + ".");
        });
    });
};

usuarioSchema.methods.resetPassword = function(cb) {
    let token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString("hex")});
    let email_destination = this.email;

    token.save(function(err) {
        if (err) return cb(err);

        let mailOptions = {
            from: "no-reply@redbicicletas.com",
            to: email_destination,
            subject: "Reseteo de password de cuenta",
            text: "Hola,\n\n" + "Por favor, para resetear el password de su cuenta haga click en este enlace: \n" + 
            "http://192.168.0.32:3000" + "\/resetPassword\/" + token.token + ".\n"
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) {return cb(err);}

            console.log("Se ha enviado un email para resetear el password a: " + email_destination + ".");
        });

        cb(null);
    });
};

module.exports = mongoose.model("Usuario", usuarioSchema);
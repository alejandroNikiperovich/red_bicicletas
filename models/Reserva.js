const moment = require("moment");
let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let reservaSchema = new Schema ({
    desde: Date,
    hasta: Date,
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta"},
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: "Usuario"}
});

reservaSchema.statics.allReservas = function (cb) {
    return this.find({}, cb);
};

reservaSchema.statics.add = function(reserva, cb) {
    return this.create(reserva, cb);
};

reservaSchema.statics.findById = function(reservaId, cb) {
    return this.findOne({_id: reservaId}, cb);
};

reservaSchema.statics.removeById = function(reservaId, cb) {
    return this.findOneAndDelete({_id: reservaId}, cb);
};

reservaSchema.statics.update = function(reservaId, cambios, cb) {
    return this.findOneAndUpdate({_id: reservaId}, cambios, cb);
};

// Cuántos días estará reservada la bicicleta
reservaSchema.methods.diasDeReserva = function() {
    return moment(this.hasta.diff(moment(this.desde), "days") + 1);
};

module.exports = mongoose.model("Reserva", reservaSchema);
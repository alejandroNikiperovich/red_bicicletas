let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
    bicicletaID: Number,
    color: String,
    modelo: String,
    ubicacion: {type: [Number], index: true}
});

bicicletaSchema.statics.allBicis = function (cb) {
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(aBici, cb) {
    return this.create(aBici, cb);
};

bicicletaSchema.statics.findById = function(aBiciId, cb) {
    return this.findOne({bicicletaID: aBiciId}, cb);
};

bicicletaSchema.statics.removeById = function(aBiciId, cb) {
    return this.findOneAndDelete({bicicletaID: aBiciId}, cb);
}

bicicletaSchema.statics.update = function(aBiciId, cambios, cb) {
    return this.findOneAndUpdate({bicicletaID: aBiciId}, cambios, cb);
}

module.exports = mongoose.model("Bicicleta", bicicletaSchema);
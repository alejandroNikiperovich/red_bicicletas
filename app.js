const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require("mongoose");
const passport = require("./config/passport");
const session = require("express-session");
const jwt = require("jsonwebtoken");

let bicicletasRouter = require('./routes/bicicletas');
let bicicletasAPIRouter = require("./routes/api/bicicletas");
let usuariosAPIRouter = require("./routes/api/usuarios");
let reservasAPIRouter = require("./routes/api/reservas");
let authAPIRouter = require("./routes/api/auth");
let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');
let usuariosRouter = require("./routes/usuarios");
let tokenRouter = require("./routes/tokens");

let app = express();
let Usuario = require("./models/Usuario");
let Token = require("./models/Token");
const store = new session.MemoryStore;

mongoose.connect("mongodb://localhost/red_bicicletas", {useUnifiedTopology: true, useNewUrlParser: true});
mongoose.Promise = global.Promise;
let db = mongoose.connection;
//Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind("Error de conexión con MongoDB"));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.set("secretkey", "JWT_PWD_!!223344");

app.use(session({
  cookie: {magAge: 240*60*60*1000},
  store: store,
  saveUninitialized: true,
  resave: "true",
  secret: "cualquier cosa no pasa nada 477447"
}))

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// Añadimos una ruta para Bicicleta
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use("/api/auth", authAPIRouter);
app.use("/api/bicicletas", validarUsuario, bicicletasAPIRouter);
app.use("/api/usuarios", usuariosAPIRouter);
app.use("/api/reservas", reservasAPIRouter);

app.use("/usuarios", usuariosRouter);
app.use("/token", tokenRouter);

app.get("/login", function(req, res) {
  res.render("session/login");
});

app.post("/login", function(req, res, next) {
  passport.authenticate("local", function(err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render("session/login", {info});

    req.logIn(usuario, function (err) {
      if (err) return next(err);
      
      return res.redirect("/");
    });
  })(req, res, next);
});

app.get("/logout", function (req, res) {
  req.logOut();
  res.redirect("/");
});

app.get("/forgotPassword", function(req, res) {
  res.render("session/forgotPassword");
});

app.post("/forgotPassword", function(req, res) {
  Usuario.findOne({email: req.body.email}, function(err, usuario) {
    if (!usuario) return res.render("session/forgotPassword", {info: {message: "No existe ese email en nuestra BBDD."}});

    usuario.resetPassword(function(err) {
      if (err) return next(err);

      console.log("session/forgotPasswordMessage");
    });
  });
});

app.get("/resetPassword/:token", function(req, res, next) {
  Token.findOne({token: req.params.token}, function(err, token) {
    if (!token) return res.status(400).send({type: "not-verified", msg: "No existe un usario asociado al token. Verifique que su token no haya expirado"});

    Usuario.findById(token._userId, function(err, usuario) {
      if (!usuario) return res.status(400).send({msg: "No existe un usuario asociado al token."});

      res.render("session/resetPassword", {errors: {}, usuario: usuario});
    });
  });
});

app.post("/resetPassword", function(req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render("session/resetPassword", {errors: {confirm_password: {message: "No coincide con el password introducido."}},
    usuario: new Usuario({email: req.body.email})});
    return;
  }

  Usuario.findOne({email: req.body.email}, function(err, usuario) {
    usuario.password = req.body.password,
    usuario.save(function(err) {
      if (err) {
        res.render("session/resetPassword", {errors: err.errors, usuario: new Usuario({email: req.body.email})});
      } else {
        res.redirect("/login");
      }
    });
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("Usuario no logueado");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers["x-access-token"], req.app.get("secretkey"), function(err, decoded) {
    if (err) {
      res.json({status: "error", message: err.message, data: null});
    } else {
      req.body.userId = decoded.id;
      console.log("jwt verify: " + decoded);
      next();
    }
  });
}

module.exports = app;

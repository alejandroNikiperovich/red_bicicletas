const express = require("express");
let usuarioController = require("../controllers/usuario");
let router = express.Router();

router.get("/", usuarioController.list);
router.get("/create", usuarioController.create_get);
router.post("/create", usuarioController.create);
router.post("/:id/delete", usuarioController.delete);
router.get("/:id/update", usuarioController.update_get);
router.post("/:id/update", usuarioController.update);

module.exports = router;
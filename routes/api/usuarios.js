let express = require("express");
let router = express.Router();
let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");

router.get("/", usuariosControllerAPI.usuarios_list);
router.post("/create", usuariosControllerAPI.usuarios_create);
router.delete("/delete", usuariosControllerAPI.usuarios_delete);
router.put("/put", usuariosControllerAPI.usuarios_put);
router.post("/reservar", usuariosControllerAPI.usuarios_reservar);

module.exports = router;
let express = require("express");
let router = express.Router();
let reservasControllerAPI = require("../../controllers/api/reservasControllerAPI");

router.get("/", reservasControllerAPI.reservas_list);
router.post("/create", reservasControllerAPI.reservas_create);
router.delete("/delete", reservasControllerAPI.reservas_delete);
router.put("/put", reservasControllerAPI.reservas_put);

module.exports = router;
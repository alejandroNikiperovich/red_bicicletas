const express = require("express");
const tokenController = require("../controllers/token");
let router = express.Router();

router.get("/confirmation/:token", tokenController.confirmationGet);

module.exports = router;